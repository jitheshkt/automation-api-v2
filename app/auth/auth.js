'use strict';
var jwt = require('jsonwebtoken');
var config = require('../../config');
module.exports = {
  verifyAccess: function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    if (token) {
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
  				return res.json({ status: false, message: 'Failed to authenticate token.' });
  			} else {
  				req.decoded = decoded;
          next();
  			}
      });
    } else {
      res.status(401).json({
        status: false,
        message: 'token not provided'
      });
    }
  },
};
