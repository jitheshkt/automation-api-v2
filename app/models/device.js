'use strict';
var mongoose 	= require('mongoose');
var Schema = mongoose.Schema;

var DeviceSchema = 	new Schema({
					userkey: {
						type: String,
						required: true
					},
					devicekey: {
						type: String,
						required: true
					},
					digital: {
						type: Object,
						required: false
					},
					analog: {
						type: Object,
						required: false
					}
}, {timestamps: true});

module.exports 	= mongoose.model('devices', DeviceSchema);
