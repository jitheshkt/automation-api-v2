'use strict';
var express = require('express');
var router = express.Router();
var userModel = require('../models/user');
var config = require('../../config');
var deviceModel = require('../models/device');
var auth = require('../auth/auth');
const log = require('simple-node-logger').createSimpleLogger('api.log');
var mqtt = require('mqtt');
module.exports = router;

var mqttServer = mqtt.connect(config.mqttServer);
mqttServer.subscribe('inTopic');

router.get('/', auth.verifyAccess, function(req, res){
  if (req.decoded) {
    userModel.findOne({
      email: req.decoded.email
    }, function(err, user) {
          deviceModel.find({userkey:user.userkey}, function(err, devices) {
          if(err) {
            log.info('unexpected error while selecting all devices at', new Date().toJSON());
            res.status(500).json({
              status: false,
              message: 'internal error occured'
            });
          } else {
            if(devices.length > 0) {
              res.status(200).json({
                status: true,
                payload: devices
              });
            } else {
              res.status(204).json({
                success: false,
                message: "no devices found"
              });
            }
          }
        });
      });
  } else {
    log.info('unexpected auth error in devices at', new Date().toJSON());
    res.status(500).json({
      status: false,
      message: 'internal auth occured'
    });
  }
});


router.get('/:devicekey', auth.verifyAccess, function(req, res, next) {
  userModel.findOne({email: req.decoded.email}, function(err, user) {
    var devicekey = req.params.devicekey;
    deviceModel.findOne({
    userkey: user.userkey,
    devicekey: devicekey
    }, function(err, device) {
      if (device) {
        res.status(200).json({
          status: true,
          payload: device
        });
      } else {
        res.status(404).json({
          status: false,
          message: "device not found"
        });
      }
    });
  });
});

router.get('/:devicekey/switches', auth.verifyAccess, function(req, res, next) {
  userModel.findOne({email: req.decoded.email}, function(err, user) {
    var devicekey = req.params.devicekey;
    deviceModel.findOne({
    userkey: user.userkey,
    devicekey: devicekey
  }, "digital", function(err, device) {
      if (device) {
        res.status(200).json({
          status: true,
          payload: device.digital
        });
      } else {
        res.status(404).json({
          status: false,
          message: "device not found"
        });
      }
    });
  });
});

router.post('/:devicekey/:switchid', auth.verifyAccess, function(req, res) {
  var devicekey = req.params.devicekey;
  var switchid = req.params.switchid;
  var switchstatus = parseInt(req.body.status);
  userModel.findOne({email: req.decoded.email}, function(err, user) {
      deviceModel.findOne({
      userkey: user.userkey,
      devicekey: devicekey
      }, "digital", function(err, device) {
      if (device) {
        if (device.digital.hasOwnProperty(switchid)) {
          let digitalObject = {};
          digitalObject[`digital.` + switchid] = switchstatus;
          deviceModel.update({
            '_id': device.id
          }, {
            '$set': digitalObject
          }, function(err, doc) {
            if(err) {
              log.info('switch update failed at ', new Date().toJSON());
              res.status(500).json({
                status: false,
                message: 'internal error occured'
              });
            }
            deviceModel.findOne({
              devicekey: devicekey
            }, function(err, device) {
              var updateddevice  = {};
              updateddevice.H   = {};
              updateddevice.H.K   = device.userkey;
              updateddevice.H.R   = device.devicekey;
              //updateddevice.H.R   = '4294939008';
              updateddevice.H.M   = 2;
              updateddevice.D     = device.digital;
              //updateddevice.A     = device.analog;

              updateddevice       = JSON.stringify(updateddevice);
              //mqtt_server.publish('server-announce', updateddevice);
              mqttServer.publish('inTopic', updateddevice);
            });
            res.status(200).json({
              status: true,
              payload: "switch status updated"
            });
          });
        } else {
          res.status(404).json({
            status: false,
            message: "switch not found"
          });
        }
      } else {
        res.status(401).json({
          status: false,
          message: "access denied for the device"
        });
      }
    });
  });
});

module.exports = router;
