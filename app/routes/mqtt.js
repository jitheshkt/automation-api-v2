'use strict';
var mqtt = require('mqtt');
var config = require('../../config');
var deviceModel = require('../models/device');
var userModel = require('../models/user');
const log = require('simple-node-logger').createSimpleLogger('api.log');

var mqttServer = mqtt.connect(config.mqttServer);
mqttServer.subscribe('outTopic');
mqttServer.subscribe('inTopic');

mqttServer.on('message', function(topic, message) {
  if (topic === 'outTopic') {
    try {
      JSON.parse(message);
    } catch (e) {
      //log.info('invalid JSON arrived through MQTT at ', new Date().toJSON());
      console.log("invalid json. skipped.");
      return;
    }

    var payload = JSON.parse(message);
    var key = payload.H.K;
    var devicekey = payload.H.R;
    var digital = payload.D;
    //var analog = payload.A;
    userModel.findOne({
      userkey: key
    }, function(err, user) {
      if (err || !user) {
        log.info('invalid user key through MQTT at ', new Date().toJSON());
        return;
      }
      deviceModel.findOne({
        userkey: key,
        devicekey: devicekey
      }, function(err, device) {
        if (!device) { //Device not exist for 'this' user. But dinkan can do magic
          deviceModel.findOne({
            devicekey: devicekey
          }, function(err, dupdevice) { //What if the device is owned by someone else?
            if (!dupdevice) {
              var newDevice = new deviceModel({
                userkey: key,
                devicekey: devicekey,
                digital: digital
                //analog : analog
              });
              newDevice.save(function(err, newdevice) {
                if (err) {
                  log.info('device registration failed for ', devicekey, 'by ', key, ' at ', new Date().toJSON());
                  return;
                }
                mqttServer.publish('inTopic', payload);
                console.log('device added');
                return;
              });
            } else {
              log.info('user ', key, ' tried to register ', devicekey, ' and failed due to access rights at ', new Date().toJSON());
              return;
            }
          });
        } else {
          // let analogObject = {};
          // Object.keys(analog).forEach(function (key) {
          //     analogObject[`analog.${key}`] = analog[key];
          // });
          var digitalObject = {};
          Object.keys(digital).forEach(function(key) {
            digitalObject[`digital.${key}`] = digital[key];
          });
          //deviceModel.update({'_id' : device.id}, {'$set': analogObject});
          deviceModel.update({
            '_id': device.id
          }, {
            '$set': digitalObject
          }, function(err, doc) {
            if (err) {
              log.info('device update failed for ', devicekey, 'by ', key, ' at ', new Date().toJSON());
              return;
            }
            mqttServer.publish('inTopic', payload);
            console.log("switches updated");
            return;
          });
        }
      });
    });
  }
});
