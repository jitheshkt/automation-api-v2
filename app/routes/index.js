'use strict';
var express = require('express');
var router = express.Router();
var userRouter = require('./user');
var deviceRouter = require('./device');
var user = require('../models/user');
var config = require('../../config');
var jwt = require('jsonwebtoken');

const log = require('simple-node-logger').createSimpleLogger('api.log');

router.get('/', function(req, res) {
  res.json({
    message: 'API v2 is up and running'
  });
});

router.post('/authenticate', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.status(406).json({
      status: false,
      message: 'you cant login without email & password'
    });
  } else {
    user.findOne({
      email: req.body.email
    }, function(err, user) {
      if (err) {
        log.info('unexpected error during user login at ', new Date().toJSON());
        res.status(500).json({
          status: false,
          message: 'internal error occured'
        });
      } else {
        if (!user) {
          log.info('invalid user tried to login at ', new Date().toJSON());
          res.status(403).json({
            status: false,
            message: 'user not found'
          });
        } else {
          user.comparePassword(req.body.password, function(err, isMatch) {
            if (isMatch && !err) {
              const payload = {
                email: user.email
              };
              var token = jwt.sign(payload, config.secret, {
                expiresIn: 86400 // expires in 24 hours
              });
              res.status(200).json({
                status: true,
                message: 'enjoy your token',
                token: token
              });
            } else {
              res.status(400).json({
                status: false,
                message: 'wrong password. auth failed'
              });
            }
          });
        }
      }
    });
  }
});

router.use('/user', userRouter);
router.use('/devices', deviceRouter);
module.exports = router;
