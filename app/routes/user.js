'use strict';
var express = require('express');
var router = express.Router();
var userModel = require('../models/user');
var shortid = require('shortid');
var config = require('../../config');
var auth = require('../auth/auth');
const log = require('simple-node-logger').createSimpleLogger('api.log');

shortid.characters(config.userkeyStrings);

router.get('/', auth.verifyAccess, function(req, res) {
  if(req.decoded) {
    userModel.findOne({email: req.decoded.email}, function(err, user) {
      if(err) {
        return res.end();
      }
      res.status(200).json({
        success: true,
        payload: {
          name: user.name,
          email: user.email,
          userkey: user.userkey,
          since: user.createdAt
        }
      });
    });
  } else {
    log.info('unexpected auth error during token validation at', new Date().toJSON());
    res.status(500).json({
      status: false,
      message: 'internal auth error occured'
    });
  }
});

router.post('/register', function(req, res) {
  if (!req.body.name || !req.body.email || !req.body.password) {
    res.status(406).json({
      status: false,
      message: 'some of the fields are missing'
    });
  } else {
    var newUser = new userModel({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      userkey: shortid.generate(),
      admin: false
    });
    newUser.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          res.status(409).json({
            status: false,
            message: 'user already exists'
          });
        } else {
          log.info('unexpected error during user creation at', new Date().toJSON());
          res.status(500).json({
            status: false,
            message: 'internal error occured'
          });
        }
      } else {
        res.status(200).json({
          status: true,
          message: 'user created'
        });
      }
    });
  }
});

module.exports = router;
